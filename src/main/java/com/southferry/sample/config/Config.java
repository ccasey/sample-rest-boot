package com.southferry.sample.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.southferry.sample.interceptor.AuthenticationInterceptor;

@Configuration
@PropertySource("classpath:sample.properties")
public class Config implements WebMvcConfigurer {

	@Autowired
	AuthenticationInterceptor authenticationInterceptor;

	/**
	 * Use the AuthenticationInterceptor to look for existing connection info,
	 * except on /s3/authenticate
	 */
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(authenticationInterceptor).excludePathPatterns("/s3/authenticate");
	}
}
