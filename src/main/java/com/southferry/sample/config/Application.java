package com.southferry.sample.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ComponentScan({ "com.southferry.sample" })
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}