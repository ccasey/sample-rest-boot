package com.southferry.sample.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpStatus;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.southferry.sample.component.CredentialsManager;
import com.southferry.sample.component.S3Credentials;
import com.southferry.sample.component.SessionCredentials;
import com.southferry.sample.service.JwtService;
import com.southferry.sample.service.JwtServiceImpl;

@Component
public class AuthenticationInterceptor implements HandlerInterceptor {

	Logger logger = Logger.getLogger(JwtServiceImpl.class);

	@Autowired
	private SessionCredentials sessionCredentials;

	@Autowired
	private JwtService jwtService;

	@Autowired
	private CredentialsManager credentialsManager;

	private final static String AUTH_HEADER = "Authorization";
	private final static String AUTH_PREFIX = "Bearer ";

	/**
	 * If they have a valid JWT and an existing connection in the pool, pass the
	 * connection info to the request scoped component for use later in the chain.
	 * If not, pass back a 401 and do not execute the controller method at all
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		String authHeader = request.getHeader(AUTH_HEADER);
		if (authHeader != null && authHeader.startsWith(AUTH_PREFIX)) {
			String jwt = authHeader.substring(AUTH_PREFIX.length());
			logger.debug("JWT Auth attempt detected with jwt: " + jwt);
			S3Credentials credentials = credentialsManager.getCredentialsPool().get(jwt);
			if (jwtService.authenticate(jwt) && credentials != null) {
				sessionCredentials.setS3Credentials(credentials);
				logger.debug("Authentication success with jwt: " + jwt);
				return true;
			}
		}
		logger.debug("Authentication failed");
		response.setStatus(HttpStatus.SC_UNAUTHORIZED);
		return false;
	}
}