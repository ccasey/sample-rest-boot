package com.southferry.sample.component;

import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.southferry.sample.service.JwtService;

@Component
public class CredentialsManager {

	Logger logger = Logger.getLogger(CredentialsManager.class);
	
	/**
	 * Because our application is RESTful and the requests are stateless, the
	 * individual connections (which are a performance drain to re-establish on
	 * request) will be stored in a map for subsequent requests. Since it's one
	 * connection per JWT I will use the JWTs as keys, and also as the guide for a
	 * scheduled purge of expired connection objects
	 * 
	 */
	private ConcurrentHashMap<String, S3Credentials> credentialsPool;
	
	@Autowired
	private JwtService jwtService;

	/**
	 * Basic initialization to create an empty credentials pool
	 */
	@PostConstruct
	public void init() {
		credentialsPool = new ConcurrentHashMap<String, S3Credentials>();
	}
	
	/**
	 * Because we are constantly adding credentials for sessions to the pool, we'd
	 * like a way to clean out that pool over time. Since the JWT has a natural
	 * expiration, and we cannot use a connection past it's expiry, this method
	 * takes advantage of that relationship.
	 * 
	 * Once a minute it iterates over the Map of open credentials and tests if the
	 * JWT is still valid (which implicitly also checks expiration). If the JWT is
	 * not valid, the credentials object is purged.
	 */
	@Scheduled(fixedDelay = 60000)
	public void cleanConnectionPool() {
		logger.debug("Attempting purge of expired credentials from pool...");
		credentialsPool.entrySet().removeIf(entry -> !jwtService.authenticate(entry.getKey()));
		logger.debug("Purge Complete");
	}

	public ConcurrentHashMap<String, S3Credentials> getCredentialsPool() {
		return credentialsPool;
	}

	public void setCredentialsPool(ConcurrentHashMap<String, S3Credentials> credentialsPool) {
		this.credentialsPool = credentialsPool;
	}
}
