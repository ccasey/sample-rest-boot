package com.southferry.sample.component;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;

/**
 * This is a class type with a very specific lifecycle. It only lives as a
 * singleton for the scope of a single request. Anywhere within the request that
 * this object is called it will refer to one instance, but all other subsequent
 * or concurrent requests will have their own respective instances.
 * 
 * In this case we use this methodology to track the specific S3 credentials
 * object from the pool which is tied to the JWT used for authentication. This
 * prevents us from having to pass the credentials into every part of the
 * various services and instead to just assign it once and use it multiple
 * times.
 */
@Component
@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class SessionCredentials {
	private S3Credentials s3Credentials;

	public S3Credentials getS3Credentials() {
		return s3Credentials;
	}

	public void setS3Credentials(S3Credentials s3Credentials) {
		this.s3Credentials = s3Credentials;
	}
}
