package com.southferry.sample.component;

import com.amazonaws.services.s3.AmazonS3;

public class S3Credentials {

	private AmazonS3 amazonS3;

	private String bucket;

	public S3Credentials(AmazonS3 amazonS3, String bucket) {
		this.amazonS3 = amazonS3;
		this.bucket = bucket;
	}

	public String getBucket() {
		return bucket;
	}

	public void setBucket(String bucket) {
		this.bucket = bucket;
	}

	public AmazonS3 getAmazonS3() {
		return amazonS3;
	}

	public void setAmazonS3(AmazonS3 amazonS3) {
		this.amazonS3 = amazonS3;
	}
}
