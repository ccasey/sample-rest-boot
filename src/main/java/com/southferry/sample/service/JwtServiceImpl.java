package com.southferry.sample.service;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.southferry.sample.component.S3Credentials;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.impl.TextCodec;

@Service
public class JwtServiceImpl implements JwtService {

	Logger logger = Logger.getLogger(JwtServiceImpl.class);

	private byte[] jwtKey;

	@Value("${sample.jwt.key}")
	private String rawKey;

	@Value("${sample.jwt.expires_in}")
	private long EXPIRES_IN;

	/**
	 * Takes the base64 encoded private JWT key from properties and converts it into
	 * the format easiest to consume for the JWT generation library.
	 * 
	 * @throws UnsupportedEncodingException
	 */
	@PostConstruct
	public void init() throws UnsupportedEncodingException {
		this.jwtKey = TextCodec.BASE64.decode(rawKey);
	}

	/**
	 * Takes a set of credentials and returns a properly signed JWT for future use.
	 * 
	 * @param S3Credentials
	 * @return jwt (String)
	 */
	@Override
	public String create(S3Credentials sessionCredentials) {
		return Jwts.builder().setId(UUID.randomUUID().toString()).setSubject(sessionCredentials.getBucket())
				.signWith(SignatureAlgorithm.HS512, jwtKey)
				.setExpiration(new Date(System.currentTimeMillis() + (EXPIRES_IN * 60000L))).compact();
	}

	/**
	 * Tests the current validity of a jwt, taking into account signature and
	 * expiration.
	 * 
	 * @param jwt (String) to test
	 * @return boolean representation of validity
	 */
	@Override
	public boolean authenticate(String jwt) {
		try {
			Jwts.parser().setSigningKey(jwtKey).parseClaimsJws(jwt);
			return true;
		} catch (SignatureException | ExpiredJwtException | UnsupportedJwtException | MalformedJwtException
				| IllegalArgumentException e) {
			return false;
		}
	}

}