package com.southferry.sample.service;

import com.southferry.sample.component.S3Credentials;

public interface JwtService {

	String create(S3Credentials sessionCredentials);

	boolean authenticate(String jwt);

}
