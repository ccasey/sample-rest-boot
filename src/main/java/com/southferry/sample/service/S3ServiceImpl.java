package com.southferry.sample.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.southferry.sample.component.CredentialsManager;
import com.southferry.sample.component.S3Credentials;
import com.southferry.sample.component.SessionCredentials;
import com.southferry.sample.exception.SampleAppException;
import com.southferry.sample.model.dto.AuthenticationResponse;

@Service
public class S3ServiceImpl implements S3Service {

	Logger logger = Logger.getLogger(S3ServiceImpl.class);

	@Autowired
	private JwtService jwtService;

	@Autowired
	private CredentialsManager credentialsManager;

	/**
	 * Will automatically be unique per request+user as per Request Scoped proxy
	 * component
	 * 
	 * @see SessionCredentials
	 */
	@Autowired
	private SessionCredentials sessionCredentials;

	/**
	 * Takes a raw byte array of file content data and a key/name. Stores the file
	 * in the bucket specified by the current sessionCredentials object as wired
	 * into the service.
	 * 
	 * @param toStore (byte[]) file content
	 * @param key (String) key to store file under
	 * @return key (String) if successfully stored
	 */
	@Override
	public String store(byte[] toStore, String key) {
		try {
			logger.debug("Storing file with key " + key + " to bucket " + 
					sessionCredentials.getS3Credentials().getBucket());
			sessionCredentials.getS3Credentials().getAmazonS3().putObject(new PutObjectRequest(
					sessionCredentials.getS3Credentials().getBucket(), key, new ByteArrayInputStream(toStore), null));
			return key;
		} catch (Exception e) {
			throw new SampleAppException("Could not store object", e);
		}
	}

	/**
	 * Takes a key corresponding to a file in the bucket specified by
	 * sessionCredentials object. Attempts to get the file from the S3 bucket and
	 * return the contents.
	 * 
	 * @param key (String) key to attempt retrieval in the bucket
	 * @return content (byte[]) of file if successful
	 */
	@Override
	public byte[] retrieve(String key) {
		logger.debug("Retrieving file with key " + key + " from bucket "
				+ sessionCredentials.getS3Credentials().getBucket());
		S3Object s3Object = sessionCredentials.getS3Credentials().getAmazonS3()
				.getObject(new GetObjectRequest(sessionCredentials.getS3Credentials().getBucket(), key));
		try (InputStream objectDataStream = s3Object.getObjectContent()) {
			return IOUtils.toByteArray(objectDataStream);
		} catch (IOException e) {
			throw new SampleAppException("Could not retrieve object", e);
		}
	}

	/**
	 * Deletes a file based on key.
	 * 
	 * @param key (String) key to attempt retrieval in the bucket
	 * @return boolean success of operation
	 */
	@Override
	public boolean delete(String key) {
		try {
			logger.debug("Deleting file with key " + key + " from bucket "
					+ sessionCredentials.getS3Credentials().getBucket());
			sessionCredentials.getS3Credentials().getAmazonS3()
					.deleteObject(sessionCredentials.getS3Credentials().getBucket(), key);
			return true;
		} catch (Exception e) {
			throw new SampleAppException("Could not delete object", e);
		}

	}

	/**
	 * 
	 * @return list of objects (List<S3ObjectSummary>) available in bucket
	 */
	@Override
	public List<S3ObjectSummary> list() {
		logger.debug("Listing bucket " + sessionCredentials.getS3Credentials().getBucket());
		return sessionCredentials.getS3Credentials().getAmazonS3()
				.listObjects(sessionCredentials.getS3Credentials().getBucket()).getObjectSummaries();
	}

	/**
	 * Takes access credentials for an S3 bucket and attempts to connect to the
	 * corresponding resource. If successful it will create and store a connection
	 * object and return a JWT corresponding to it for subsequent authentication and
	 * use.
	 * 
	 * @param accessId
	 *            - ID String from Amazon AWS user specification
	 * @param accessKey
	 *            - Secret key from Amazon AWS user specification
	 * @param region
	 *            - bucket region in the AWS API format (e.g. us-east-2)
	 * @param bucket
	 *            - the unique bucket name which the above credentials must have 
	 *            access to
	 * @return AuthenticationResponse - An object containing authentication status
	 *         and a jwt
	 */
	@Override
	public AuthenticationResponse authenticate(String accessId, String accessKey, String region, String bucket) {
		try {

			logger.debug("Attempting authentication for bucket " + bucket);
			// initialize connection
			BasicAWSCredentials credentials = new BasicAWSCredentials(accessId, accessKey);
			AmazonS3 amazonS3 = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(credentials)).withRegion(region).build();

			// test connection
			amazonS3.listObjects(bucket);

			logger.debug("Authentication for bucket " + bucket + " successful");

			// generate and store credentials object and JWT for user
			S3Credentials sessionCredentials = new S3Credentials(amazonS3, bucket);
			String jwt = jwtService.create(sessionCredentials);
			credentialsManager.getCredentialsPool().put(jwt, sessionCredentials);

			return new AuthenticationResponse(true, jwt);
		} catch (Exception e) {
			logger.error("Authentication failed for bucket " + bucket, e);
			return new AuthenticationResponse(false);
		}
	}
}
