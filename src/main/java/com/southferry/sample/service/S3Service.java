package com.southferry.sample.service;

import java.util.List;

import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.southferry.sample.model.dto.AuthenticationResponse;

public interface S3Service {

	String store(byte[] toStore, String key);

	byte[] retrieve(String key);

	boolean delete(String key);

	AuthenticationResponse authenticate(String accessId, String accessKey, String region, String bucket);

	List<S3ObjectSummary> list();
}
