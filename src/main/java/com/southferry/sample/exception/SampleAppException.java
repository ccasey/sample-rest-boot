package com.southferry.sample.exception;

public class SampleAppException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public SampleAppException(String message) {
		super(message);
	}

	public SampleAppException(String message, Exception e) {
		super(message, e);
	}

}
