package com.southferry.sample.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.southferry.sample.exception.SampleAppException;
import com.southferry.sample.model.dto.AuthenticationRequest;
import com.southferry.sample.model.dto.AuthenticationResponse;
import com.southferry.sample.model.dto.StoreResponse;
import com.southferry.sample.model.dto.SuccessResponse;
import com.southferry.sample.service.S3Service;

@RestController
public class S3Controller {

	@Autowired
	private S3Service s3Service;

	/**
	 * Takes a request from a user to authenticate with an Amazon S3 Bucket. Request
	 * contains all relevant AWS user credentials and bucket information
	 * 
	 * This is the only method that does not require a valid JWT in header to execute.
	 * 
	 * 
	 * @param AuthenticationRequest
	 * @return AuthenticationResponse
	 */
	@RequestMapping(value = "/s3/authenticate", method = RequestMethod.POST)
	public @ResponseBody AuthenticationResponse authenticate(@RequestBody AuthenticationRequest authenticationRequest) {
		return s3Service.authenticate(authenticationRequest.getAccessId(), authenticationRequest.getAccessKey(),
				authenticationRequest.getRegion(), authenticationRequest.getBucket());
	}

	/**
	 * Provides a list of all available objects in the bucket. 
	 * 
	 * Requires a JWT in header to be executed or it will return a 401.
	 * 
	 * 
	 * @return List of S3 Object Summaries
	 */
	@RequestMapping(value = "/s3/", method = RequestMethod.GET)
	public @ResponseBody List<S3ObjectSummary> list() {
		return s3Service.list();
	}

	/**
	 * Uploads a file to the S3 Bucket. Requires the Multi-part form submission
	 * protocol from the client side.
	 * 
	 * Requires a JWT in header to be executed or it will return a 401.
	 * 
	 * @param file to be uploaded
	 * @return StoreResponse (a status object to inform the user of result)
	 * @throws IOException
	 */
	@RequestMapping(value = "/s3/", method = RequestMethod.POST)
	public @ResponseBody StoreResponse store(@RequestParam("file") MultipartFile file) throws IOException {

		if (file.isEmpty())
			throw new SampleAppException("File was not provided");

		return new StoreResponse(true, s3Service.store(file.getBytes(), file.getOriginalFilename()));
	}

	/**
	 * Streams the binary file contents of a file in the bucket specified by key.
	 * 
	 * Requires a JWT in header to be executed or it will return a 401.
	 * 
	 * @param response (autoinjected by framework)
	 * @param key of desired object
	 * @throws IOException
	 */
	@RequestMapping(value = "/s3/{key}", method = RequestMethod.GET)
	public void retrieve(HttpServletResponse response, @PathVariable(value = "key") String key) throws IOException {

		if (StringUtils.isEmpty(key))
			throw new SampleAppException("No Key Provided");

		response.setHeader("Cache-Control", "no-store");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		ServletOutputStream responseOutputStream = response.getOutputStream();
		responseOutputStream.write(s3Service.retrieve(key));
		responseOutputStream.flush();
	}

	/**
	 * Deletes an object with the specified key from the bucket.
	 * 
	 * Requires a JWT in header to be executed or it will return a 401.
	 * 
	 * @param key of object to be deleted
	 * @return SuccessResponse (a status object to inform the user of result)
	 * @throws IOException
	 */
	@RequestMapping(value = "/s3/{key}", method = RequestMethod.DELETE)
	public SuccessResponse delete(@PathVariable(value = "key") String key) throws IOException {
		return new SuccessResponse(s3Service.delete(key));
	}

}