package com.southferry.sample.model.dto;

public class StoreResponse {

	private Boolean success;

	private String key;

	public StoreResponse(Boolean success, String key) {
		super();
		this.success = success;
		this.key = key;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

}
