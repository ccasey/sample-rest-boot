package com.southferry.sample.model.dto;

public class AuthenticationResponse {

	private Boolean succcess;

	private String jwt;

	public AuthenticationResponse(Boolean succcess) {
		this.succcess = succcess;
	}

	public AuthenticationResponse(Boolean succcess, String jwt) {
		this.succcess = succcess;
		this.jwt = jwt;
	}

	public Boolean getSucccess() {
		return succcess;
	}

	public void setSucccess(Boolean succcess) {
		this.succcess = succcess;
	}

	public String getJwt() {
		return jwt;
	}

	public void setJwt(String jwt) {
		this.jwt = jwt;
	}

}
