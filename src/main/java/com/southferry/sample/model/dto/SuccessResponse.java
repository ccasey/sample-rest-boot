package com.southferry.sample.model.dto;

public class SuccessResponse {
	private Boolean success;

	public SuccessResponse(boolean success) {
		this.success = success;
	}

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

}
