# S3 REST Wrapper (Sample Code Project)

For this sample of code I made a REST API that wraps and simplifies Amazon's S3 Bucket API. It's a basic CRUD that operates on one bucket at a time.

It has no need for local persistence like a database or cache because it saves nothing except in S3. You authenticate with an endpoint using your bucket credentials and are given a JWT in response. That JWT ties you to an open connection stored in the application's memory for the life of your session. All requests after that simply use the JWT in the header and the rest (including specified bucket) is assumed from the initial authentication request.

The application is in Java but I used an embedded server so there is no need to deploy it to tomcat or anything. 

## Getting Started

Building and launching this application should be relatively hassle-free, as it does not need a separate server, but there are a few things you need to install first if you don't have them. 

#### Prerequisites

Java 8 JDK ([find here](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html))

Maven 3+ ([find here](https://maven.apache.org/download.cgi) - Build and Dependency Management tool)


#### Installing

Once you have Java and Maven installed and configured, building and launching the app should be simple.

Navigate to the root of the project and type:

```bash
mvn clean install
java -jar target/sample-app-0.1.0.jar
```

If all goes well the application should launch on port 8080. 

## API Endpoints

The following are the endpoints available in the system. We will use Postman to demonstrate use but any client will work.

#### POST /s3/authenticate

This endpoint takes a JSON object containing the relevant S3 connection details and returns an object with a status and a JWT (if successful). You will need to run this call before using any other calls because it is the only one that does not require a JWT.

The structure of the JSON authentication object to send is:

```javascript
{
	"accessId":"",
	"accessKey":"",
	"region":"",
	"bucket":""
}

```

![Authentication Demo](https://s3.amazonaws.com/sfit-image/demo-app-project/auth.png)

The JWT retrieved from this call will be used in the header of all subsequent calls.

#### GET /s3/

This endpoint lists all of the available files in the bucket. The JWT in the header controls the bucket and authentication details. The JWT should go in a header called "Authorization" prefixed with the string "Bearer " as seen below:

![List Demo](https://s3.amazonaws.com/sfit-image/demo-app-project/list.png)

#### GET /s3/{key}

With this endpoint we can download a file from the bucket by specifying the key of the file. The key is the filename as initially uploaded.

![Get Demo](https://s3.amazonaws.com/sfit-image/demo-app-project/get.png)

#### POST /s3/

This is how we add individual files. Use a form-data protocol with the key 'file' for the binary you are uploading as seen below:

![Post Demo](https://s3.amazonaws.com/sfit-image/demo-app-project/post.png)

#### DELETE /s3/{key}

This is the method for deleting files. Similar to the GET /s3/{key} we use the key of the file in the bucket to target. 

## Structure

If you are unfamiliar with Spring or Spring Boot applications I can break down the sections a bit to make it easier to digest. The sections below are the packages under com.southferry.sample

#### controller

Typical MVC Controllers. They specify the path, method, parameters, and return types and also contain the calls to logic. The only relevant controller is S3Controller which contains the basic crud methods as well as the authentication method.

#### config

This is essentially where the application bootstraps. This defines, through annotations and methods, the resources available to the application.

#### component

These are beans of various scopes which are used in the application but do not usually themselves constitute a stateless service contract.

#### service

These are my stateless services (and their interfaces). JwtService contains all the logic for creating and parsing JWTs and S3Service contains all the amazon API calls, as well as a special authentication method.

#### interceptor

These can be configured to run before or after controller calls. In this case I use it for authentication and authorization for every call except authenticate. 

#### model.dto

Just some POJOs and wrappers used for clean data transfer to and from the API

## Author

* **Cliff Casey** - [South Ferry IT Consulting](https://www.southferrysystems.com)
